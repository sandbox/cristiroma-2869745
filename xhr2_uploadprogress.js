/**
 * @file
 * Provides JavaScript additions to the managed file field type.
 *
 * This file provides xhr2 progress bar support (if available).
 *
 * Pitfalls:
 * 1. The beforeSend function had to be overriden from core to add support for setting up XHR uploadprogress.
 * 2. It might not work (NOT TESTED) on older browsers like IE 8, 9 etc.
 *
 * @see core/misc/ajax.js
 */

(function ($, Drupal) {

    'use strict';

    $.extend(Drupal.Ajax.prototype, /** @lends Drupal.ProgressBar */ {
        /**
         * Override Drupal core beforeSend (copied from ajax.js)
         *
         * @param {XMLHttpRequest} xmlhttprequest
         *   Native Ajax object.
         * @param {object} options
         *   jQuery.ajax options.
         */
        beforeSend: function (xmlhttprequest, options) {
            // For forms without file inputs, the jQuery Form plugin serializes the
            // form values, and then calls jQuery's $.ajax() function, which invokes
            // this handler. In this circumstance, options.extraData is never used. For
            // forms with file inputs, the jQuery Form plugin uses the browser's normal
            // form submission mechanism, but captures the response in a hidden IFRAME.
            // In this circumstance, it calls this handler first, and then appends
            // hidden fields to the form to submit the values in options.extraData.
            // There is no simple way to know which submission mechanism will be used,
            // so we add to extraData regardless, and allow it to be ignored in the
            // former case.
            if (this.$form) {
                options.extraData = options.extraData || {};

                // Let the server know when the IFRAME submission mechanism is used. The
                // server can use this information to wrap the JSON response in a
                // TEXTAREA, as per http://jquery.malsup.com/form/#file-upload.
                options.extraData.ajax_iframe_upload = '1';

                // The triggering element is about to be disabled (see below), but if it
                // contains a value (e.g., a checkbox, textfield, select, etc.), ensure
                // that value is included in the submission. As per above, submissions
                // that use $.ajax() are already serialized prior to the element being
                // disabled, so this is only needed for IFRAME submissions.
                var v = $.fieldValue(this.element);
                if (v !== null) {
                    options.extraData[this.element.name] = v;
                }
            }

            // Disable the element that received the change to prevent user interface
            // interaction while the Ajax request is in progress. ajax.ajaxing prevents
            // the element from triggering a new request, but does not prevent the user
            // from changing its value.
            $(this.element).prop('disabled', true);

            if (!this.progress || !this.progress.type) {
                return;
            }

            // Insert progress indicator.
            var progressIndicatorMethod = 'setProgressIndicator' + this.progress.type.slice(0, 1).toUpperCase() + this.progress.type.slice(1).toLowerCase();
            if (progressIndicatorMethod in this && typeof this[progressIndicatorMethod] === 'function') {
                if (progressIndicatorMethod === 'setProgressIndicatorXhr2') {
                    this[progressIndicatorMethod].call(this, options);
                }
                else {
                    this[progressIndicatorMethod].call(this);
                }
            }
        },


        setProgressIndicatorXhr2: function (options) {
            var progressBar = new Drupal.ProgressBar('ajax-progress-' + this.element.id, $.noop, this.progress.method, $.noop);
            if (this.progress.message) {
                progressBar.setProgress(-1, this.progress.message);
            }
            progressBar.startMonitoring(null, this.progress.interval || 1000);

            this.progress.element = $(progressBar.element).addClass('ajax-progress ajax-progress-bar');
            this.progress.object = progressBar;
            $(this.element).after(this.progress.element);

            options.xhr = function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener('progress', function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = Math.ceil((evt.loaded / evt.total) * 100);
                        // Update the progress bar
                        progressBar.setProgress(percentComplete, 'Uploading ...');
                    }
                }, false);
                return xhr;
            };
        }
    });

    $.extend(Drupal.ProgressBar.prototype, /** @lends Drupal.ProgressBar */ {
        /* This method does nothing since no server interaction is required */
        sendPing: function () {}
    });

})(jQuery, Drupal);

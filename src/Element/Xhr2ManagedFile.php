<?php

namespace Drupal\xhr2_uploadprogress\Element;

use Drupal\file\Element\ManagedFile;

/**
 * Provides an AJAX/progress aware widget for uploading and saving a file.
 *
 * @FormElement("xhr2_managed_file")
 */
class Xhr2ManagedFile extends ManagedFile {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $ret = parent::getInfo();
    $ret['#attached']['library'][] = 'xhr2_uploadprogress/uploadprogress';
    $ret['#progress_indicator'] = 'xhr2';
    return $ret;
  }

}

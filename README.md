# xhr2_uploadprogress
 
This module adds an alternative to the standard core-provided *bar* upload progress widget with new support HTML 5 support in browsers via *xhr.upload.progress*.

**Note**: This is an API module and is targeted towards module developers.

**Pitfalls**: 
 
   1. The beforeSend function had to be overriden from core to add support for setting up XHR uploadprogress (see xhr2_uploadprogress.js)
   2. It might not work (NOT TESTED) on older browsers like IE 8, 9 etc.

# How to use

1. Install the module as usually: https://www.drupal.org/node/895232
2. In your form class, you configure the field #type as xhr2_managed_file, example:   

    ```php
      $form['files'] = [
        '#type' => 'xhr2_managed_file',
        '#title' => t('Add files'),
        '#progress_indicator' => 'xhr2',
        '#required' => true,
        '#multiple' => true,
        '#upload_validators' => ['file_validate_extensions' => [$field_definition->getSetting('file_extensions')]],
        '#weight' => 3,
      ];
   ```

When you upload files, the system should display a progress bar that tracks the form submission progress.
